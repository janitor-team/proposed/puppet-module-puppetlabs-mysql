require 'spec_helper'

describe command('puppet apply --detailed-exitcodes --execute "include mysql::client"') do
  its(:exit_status) { should eq 2 }
end

describe package('mariadb-client') do
  it { should be_installed }
end

describe command('puppet apply --detailed-exitcodes --execute "include mysql::server"') do
  its(:exit_status) { should eq 2 }
end

describe package('mariadb-server') do
  it { should be_installed }
end

describe service('mysql') do
  it { should be_running }
end
